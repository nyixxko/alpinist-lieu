package com.example.nyiko.alpinistlieu;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    //Bring in Location Manager & Location Listener Classes
    LocationManager locationManager;

    LocationListener locationListener;

    //When the user gives the permission on request,
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //Check whether the permission is granted
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

            startRecording();

        }

    }

    //When the permission is granted, it starts recording
    public void startRecording() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){

            //Getting system service from Current Location
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        }
    }

    //Create a method to update locatin data info or data...
    public void updateLocationData(Location location) {

        Log.i("LocationData", location.toString());

        //Find various text view
        TextView latTextView = (TextView) findViewById(R.id.latTextView);

        TextView lonTextView = (TextView) findViewById(R.id.lonTextView);

        TextView altTextView = (TextView) findViewById(R.id.altTextView);

        //Set each of lon, lat and alt appropriately to be appeared on the app
        latTextView.setText("Latitude: " + location.getLatitude());

        lonTextView.setText("Longtitude: " + location.getLongitude());

        altTextView.setText("Altitude: " + location.getAltitude());

        //Reverse geo coding from Latitude & Longitude to get ADDRESS by using user's locale
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

        //Listing the addresses and it could fail...So surround with try and catch
        try {

            //If it does fail, below message will display rather than leaving it empty
            String addressDescription = "Couldn't find the address";

            List<Address> listAddresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); //Only want once...hence "1"

            //Check if there's indeed addresses
            if (listAddresses != null && listAddresses.size() > 0) {

                //Get 1st item in the info...hence "0"
                Log.i("PlaceInfo", listAddresses.get(0).toString());

                //Add the address description to empty string once getting data/info is successful
                addressDescription = " ";

                //Getting each item in the list of address by using Geocoder Class

                //Getting the no of the street address
                if (listAddresses.get(0).getSubThoroughfare() != null) {

                    addressDescription += listAddresses.get(0).getSubThoroughfare() + " ";
                }

                //Getting the street address of the location
                if (listAddresses.get(0).getThoroughfare() != null) {

                    addressDescription += listAddresses.get(0).getThoroughfare() + "\n";
                }

                //Getting the postal code
                if (listAddresses.get(0).getPostalCode() != null) {

                    addressDescription += listAddresses.get(0).getPostalCode() + "\n";
                }

                //Getting the country name
                if (listAddresses.get(0).getCountryName() != null) {

                    addressDescription += listAddresses.get(0).getCountryName() + "\n";
                }

            }
            //Update Address Text View with Geocoder
            TextView addressTextView = (TextView) findViewById(R.id.addressDesTextView);

            addressTextView.setText(addressDescription);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startRecording();

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                updateLocationData(location);

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        //Check if the SDK version is less than 23, location manager will start requesting location updates
        if (Build.VERSION.SDK_INT < 23) {

            //Update location with no minimum time or distance requirement by using GPS Provider
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

        } else {

            //Check whether or not if the permission is granted
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                //If the permission is not granted, need to ask for permission by using activity
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else {

                //If the permission is granted, can start listening
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                //Last known location
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                //Quick check on last known location...that's if the location is not known then call the method updateLocationData to update
                if (location != null) {

                    //To update AlpinistLieu page
                    updateLocationData(location);
                }
            }

        }
    }
}
